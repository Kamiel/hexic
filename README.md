## Requirements

You need **Haskell** **Platform** and fresh `cabal-install`.

## Installation

```
$ git clone --depth 1 https://bitbucket.org/Kamiel/hexic
$ cd hexic
$ cabal sandbox init
$ cabal install --only-dependencies
$ cabal build
```

## Usage

```
$ ./dist/build/hexic/hexic
```

***

*Enjoy!*
