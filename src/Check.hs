module Check where

import Grid
import Navigation
import Process

import Data.Maybe

-- find near location the cell with same color
-- and not checked yet
-- with common neighbor which satisfies condition
findNear       :: ProcessGrid -> Location -> (ProcessCell -> Bool) -> [Location]
findNear g l p = findNear'
  where
    sameColor = case getProcess g l of
                 Nothing -> []
                 Just c  -> filter (sameColor' c) $ near l
    sameColor' c t = case getProcess g t of
                      Nothing -> False
                      Just e  -> color e == color c
                              && not (checked e)
    findNear' = let neighbors n' = map fromJust $ filter isJust $ neighbors'' n'
                    neighbors'' n'' = map (getProcess g) $ neighbor n'' l
                in flip filter sameColor $ \n -> any p $ neighbors n

-- find all nearest with same color that have common with same color
findPair :: ProcessGrid -> Location -> [Location]
findPair g l = case getProcess g l of
                Nothing -> []
                Just c  -> findNear g l $ \n -> color n == color c

-- first of them take to make claster basis
pair :: ProcessGrid -> Location -> Maybe Location
pair g l = case findPair g l of
            []   -> Nothing
            p:ps -> Just p

-- find all nearest with same color that have common already checked
-- because claster cells should have 2 common edges
newToCluster :: ProcessGrid -> Location -> [Location]
newToCluster g l = findNear g l $ \n -> checked n

check     :: ProcessTurn -> [Location] -> ProcessTurn
check (g, p) (l : ls) = check check' ls
  where
    check' = case pair g l of
              Nothing -> (g, p)
              Just n  -> findCluster (g, p) [l,n]
    findCluster s       [] = s
    findCluster (g', p') f = let g'' = mark g f
                                 f'  = concat $ map (newToCluster g'') f
                                 p'' = p' + kPointPrice * length f'
                             in findCluster (g'', p'') f'
