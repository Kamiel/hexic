module Grid where

import Control.Monad
import Data.List
import System.Random

data Cell = Red
          | Orange
          | Yellow
          | Green
          | Cyan
          | Blue
          | Magenta
          deriving (Enum, Bounded, Eq)

type Grid = [[Cell]]

instance Show Cell where
  show = show . fromEnum

instance Random Cell where
  randomR (a, b) g = case randomR (fromEnum a, fromEnum b) g of
    (x, g') -> (toEnum x, g')
  random g = randomR (minBound, maxBound) g

generate     :: Int -> Int -> IO Grid
generate x y = do
  g <- newStdGen
  replicateM x $ replicateM y $ randomIO
