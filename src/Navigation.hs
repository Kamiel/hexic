module Navigation where

import Grid

import Data.Sequence
  ( Seq(..)
  , fromList
  , update
  )

import Data.Foldable (toList)
import Data.List (find)

data Direction = Up
               | UpRight
               | DownRight
               | Down
               | DownLeft
               | UpLeft
               deriving (Enum, Bounded)

data Location = Location { row    :: !Int
                         , column :: !Int
                         } deriving (Show, Eq)

valid :: [[item]] -> Location -> Bool
valid g Location { row    = i
                 , column = j
                 } =  i >= 0
                   && j >= 0
                   && i < length g
                   && j < length r
  where
    r = g !! i

-- yeah, C-style a little there

getItem :: [[item]] -> Location -> Maybe item
getItem g l | valid g l = Just $ g !! row l !! column l
            | otherwise = Nothing

getCell :: Grid -> Location -> Maybe Cell
getCell = getItem

updateItem :: [[item]] -> Location -> item -> [[item]]
updateItem g l e | valid g l = g'
                 | otherwise = g
  where
    i  = row l
    j  = column l
    r  = g !! i
    r' = updateList j e  r
    g' = updateList i r' g
    updateList x m s = toList $ update x m  $ fromList s

updateCell :: Grid -> Location -> Cell -> Grid
updateCell = updateItem

nextTo :: Direction -> Location -> Location
nextTo d Location { row    = i
                  , column = j
                  } = uncurry next offset
  where
    offset | odd i     = (j - 1, j)
           | otherwise = (j, j + 1)
    next l r = case d of
                Up        -> Location { row = i - 2, column = j }
                UpRight   -> Location { row = i - 1, column = r }
                DownRight -> Location { row = i + 1, column = r }
                Down      -> Location { row = i + 2, column = j }
                DownLeft  -> Location { row = i + 1, column = l }
                UpLeft    -> Location { row = i - 1, column = l }

near   :: Location -> [Location]
near l = [l' | d <- [minBound..]
             , let l' = nextTo d l]

-- return neighbors common to two locations
neighbor :: Location -> Location -> [Location]
neighbor x y = [n | n <- near x, n' <- near y, n == n']
