module Draw where

import Grid

import Control.Monad

draw   :: Grid -> IO ()
draw g = draw' True True g
  where
    draw' fstp oddp []       | fstp      = putStrLn ""
                             | otherwise = let
                                 count = length $ head g
                                 in drawBottom (not oddp) count
    draw' fstp oddp (r : rs) = do
      when oddp $ do
        putStr $ if fstp then "  " else "\\_"
      drawRow r
      unless oddp $ do
        putStr "_/"
      putStrLn ""
      draw' False (not oddp) rs

drawRow          :: [Cell] -> IO ()
drawRow []       = return ()
drawRow (h : hs) = do
  putStr "/"
  putStr $ show h
  putStr "\\"
  unless (null hs) $ do
    putStr "_"
  drawRow hs

drawBottom        :: Bool -> Int -> IO ()
drawBottom oddp c = do
  when oddp $ do
    putStr "  "
  replicateM_ c $ do
    putStr "\\_/ "
  putStrLn ""
