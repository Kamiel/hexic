module Process where

import Grid
import Navigation

import Data.Maybe
  ( isNothing
  , fromJust
  )
import Control.Applicative

type Point = Int

-- number of points for each cell in cluster
-- should be mutiply, not sum.
kPointPrice :: Point
kPointPrice = 3

data ProcessCell = Process { color   :: !Cell
                           , checked :: !Bool
                           } deriving (Eq)

type ProcessGrid = [[ProcessCell]]

type ProcessTurn = (ProcessGrid, Point)
type Turn = (Grid, Point)

toProcessGrid   :: Grid -> ProcessGrid
toProcessGrid = (map . map) toProcess
  where
    toProcess c = Process { color   = c
                          , checked = False
                          }

-- mark list locations as checked
mark            :: ProcessGrid -> [Location] -> ProcessGrid
mark g []       = g
mark g (l : ls) = mark mark' ls
  where
    mark' = case getItem g l of
             Nothing -> g
             Just c  -> updateProcess g l c

getProcess :: ProcessGrid -> Location -> Maybe ProcessCell
getProcess = getItem

updateProcess :: ProcessGrid -> Location -> ProcessCell -> ProcessGrid
updateProcess = updateItem

process :: Turn -> Turn
process s@(g, p) = s -- TODO:
-- get all variants by rotate each triangle cell combination
-- for each combination process it:
-- check clusters for each of 3 cell
-- if clasters detected check all falled down
-- currently exist cells on new clusters
-- return from process of each combination
-- sum of points
-- take variant with greatest point number
-- print it
-- generate new falled down if necessary
-- then calculate process for next turn
-- stop when none turn with point > 0 will be found
