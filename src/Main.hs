module Main where

import Helper

import Data.Maybe

main = let
  rows    = 17
  columns = 5
  in do
    print "Let's hexic!"
    putStrLn ""
    grid <- generate rows columns
    draw grid
    let start = Location { row    = 0
                         , column = 0 }
    print $ fromJust $ getCell grid $ nextTo Down $ nextTo UpRight $ nextTo DownRight $ nextTo DownLeft start
