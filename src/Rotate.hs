module Rotate where

import Grid
import Navigation

import Data.Maybe

data Rotate = Clockwise | CounterClockwise

rotate :: Grid -> Rotate -> Location -> Location -> Location -> Grid
rotate g r x y z = if isNothing x' || isNothing y' || isNothing z' then g
                   else case r of
                         Clockwise        -> triangle z'' x'' y''
                         CounterClockwise -> triangle y'' z'' x''
  where
    x' = getCell g x
    y' = getCell g y
    z' = getCell g z
    x'' = fromJust x'
    y'' = fromJust y'
    z'' = fromJust z'
    triangle a b c = updateCell (updateCell (updateCell g x a) y b) z c

-- return all possible turn variants after rotating
turns   :: Grid -> [Grid]
turns g | null g    = [g]
        | otherwise = let j = length g
                          i = length $ head g
                      in concat $ map rotation $ [l | i' <- [0 .. i - 1],
                                                      j' <- [0 .. j - 2],
                                                      let l = Location { row    = i'
                                                                       , column = j'
                                                                       }]
  where
    j = length g
    i = length $ head g
    locations  = [l | i' <- [0 .. i - 1],
                      j' <- [0 .. j - 2],
                      let l = Location { row    = i'
                                       , column = j'
                                       }]
    rotation x = let d = nextTo Down      x
                     l = nextTo DownLeft  x
                     r = nextTo DownRight x
                 in [ rotate g Clockwise        x d l
                    , rotate g CounterClockwise x d l
                    , rotate g Clockwise        x r d
                    , rotate g CounterClockwise x r d
                    ]
